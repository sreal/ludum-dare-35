﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using domain;
using System;

public class GuestButton : MonoBehaviour
{
    public GameManager gameManager;
    Image ProfileImage;
    Text NameText;
    Button Button;
    Person person;

    public void SetUser(Person person) {
        this.person = person;
        NameText.text = person.Name;
        ProfileImage.sprite = Resources.Load<Sprite>(String.Format("Sprites/{0}", person.ImageSprite));
    }

    public void LoadProfile()
    {
        gameManager.LoadProfile(this.person);
    }

    void Awake () {
        gameManager = Camera.main.GetComponent<GameManager>();
        NameText = GetComponentInChildren<Text>();
        ProfileImage = GetComponentInChildren<Image>();

        Button = GetComponentInChildren<Button>();
        Button.onClick.AddListener(LoadProfile);
    }
}
