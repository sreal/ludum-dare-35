using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using domain;

public class Profile : MonoBehaviour
{
    GameManager gameManager;
    Image Image;
    Text Title;
    Text Summary;
    Button AccuseButton;

    public void SetUser(Person person)
    {
        var isGuestAreAccusable = person.Role == GuestRole.Guest;
        SetUser(person.Name, DetailGenerator.GetSummary(person), person.ImageSprite, isGuestAreAccusable);
    }

    public void SetUserAsWin(Person person)
    {
        SetUser("ARG!! You found me!#@!", DetailGenerator.GetWin(person), person.ImageSprite);
    }

    public void SetUserAsLoss(Person person)
    {
        SetUser("This is akward", DetailGenerator.GetLoss(person), person.ImageSprite);
    }

    void Awake()
    {
        gameManager = Camera.main.GetComponent<GameManager>();
        Title = GetComponentsInChildren<Text>().Single(x => x.name == "Title");
        Summary = GetComponentsInChildren<Text>().Single(x => x.name == "Summary");
        Image = GetComponentsInChildren<Image>().Single(x => x.name == "Image");
        AccuseButton = GetComponentsInChildren<Button>().Single(x => x.name == "AccuseButton");
        AccuseButton.onClick.AddListener(() => gameManager.Accuse());
    }

    private void SetUser(string name, string summary, string spriteName, bool isAccusable = false)
    {
        Title.text = name;
        Summary.text = summary;
        Image.sprite = Resources.Load<Sprite>(String.Format("Sprites/{0}", spriteName));
        AccuseButton.gameObject.SetActive(isAccusable);
    }
}
