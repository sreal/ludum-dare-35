using System;
using System.Collections.Generic;

namespace domain
{
    public class PartyFactory
    {
        PersonFactory _personFactory;

        public PartyFactory(PersonFactory personFactory)
        {
            _personFactory = personFactory;
        }

        public Party Create()
        {
            var count = 6;
            var attendants = new List<Person>
            {
                _personFactory.CreateHost(),
                _personFactory.CreateDoorman()
            };
            for (int i = 0; i < count / 2; i++)
            {
                Person p1;
                if (i == 0)
                    p1 = _personFactory.CreateGuestShapeshifter();
                else
                    p1 = _personFactory.CreateGuest();

                var p2 = _personFactory.CreateGuest();
                p2.Partner = p1;
                p1.Partner = p2;
                attendants.Add(p1);
                attendants.Add(p2);
            }

            var party = new Party();
            party.Attendants = attendants;

            return party;
        }
    }


    public class PersonFactory
    {
        private Person Create(GuestRole role)
        {
            return new Person()
            {
                Id = Guid.NewGuid(),
                Name = DetailGenerator.GetName(role),
                Role = role,
                IsShifter = false,
                ImageSprite = DetailGenerator.GetImage(role)
            };
        }
        private Shifter CreateShifter()
        {
            return new Shifter()
            {
                Id = Guid.NewGuid(),
                Name = DetailGenerator.GetName(GuestRole.Guest),
                Role = GuestRole.Guest,
                IsShifter = true,
                ImageSprite = DetailGenerator.GetImage(GuestRole.Guest)
            };
        }

        public Shifter CreateGuestShapeshifter()
        {
            return this.CreateShifter();
        }

        public Person CreateGuest()
        {
            return this.Create(GuestRole.Guest);
        }
        public Person CreateHost()
        {
            return this.Create(GuestRole.Host);
        }
        public Person CreateDoorman()
        {
            return this.Create(GuestRole.Doorman);
        }

    }
}
