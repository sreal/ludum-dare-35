using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace domain
{
    public class Shifter : Person
    {
        public bool MissingFromGuestList { get; set; }
        public bool WrongPartner { get; set; }
    }

    public class Person
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ImageSprite { get; set; }
        public GuestRole Role { get; set; }
        public bool IsShifter { get; set; }
        public Person Partner { get; set; }

        public string Summary {
            get {
                return String.Format(@"I came to this party with {0}.", Partner.Name);
            }
        }

        public Person Clone()
        {
            return new Person()
            {
                Id = Id,
                Name = Name,
                ImageSprite = ImageSprite,
                Role = Role,
                IsShifter = IsShifter,
                Partner = Partner
            };
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Person p = (Person)obj as Person;
            if (p == null)
                return false;

            return Id == p.Id;
        }
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    public class Party
    {
        IList<Person> _guests;
        IList<Person> _guestList;
        Person _host;
        Person _doorman;
        Shifter _shifter;

        IList<Person> _attendents;
        public IList<Person> Attendants
        {
            get { return _attendents; }
            set
            {
                _attendents = value;
                var guests = value.Where(x => x.Role == GuestRole.Guest);

                _guestList = guests.Select(x => x.Clone()).ToList();
                _guests = guests.Select(x => x.Clone()).OrderBy(_ => Guid.NewGuid()).ToList();

                _host = value.Single(x => x.Role == GuestRole.Host);
                _doorman = value.Single(x => x.Role == GuestRole.Doorman);
                _shifter = (Shifter)value.Single(x => x.IsShifter);
            }
        }

        public IList<Person> GuestList { get { return _guestList; } }
        public Person Host { get { return _host; } }
        public Person Doorman { get { return _doorman; } }
        public IList<Person> Guests { get { return _guests; } }
        public Shifter Shifter { get { return _shifter; } }

        public void InitalizeShifter()
        {
            if (_attendents == null)
            {
                throw new ApplicationException("Attendents not added.");
            }

            if (_shifter.MissingFromGuestList)
            {
                _guestList.Remove(_shifter);
            }

            if (_shifter.WrongPartner)
            {
                var shifter = _guests.Single(x => x.IsShifter);
                shifter.Partner = _guests.OrderBy(_ => Guid.NewGuid()).First(x => !x.IsShifter || x != shifter.Partner);
            }
        }

        public string DebugParty() {
            return String.Format(@"
guest: {0}
guestNames: {1}

guestList: {2}
guestList Names: {3}

shifter: {4} ",
                                 this.Guests.Count,
                                 String.Join(",", this.Guests.Select(x => x.Name).ToArray()),
                                 this.GuestList.Count,
                                 String.Join(",", this.GuestList.Select(x => x.Name).ToArray()),
                                 this.Shifter.Name);
        }

    }
}
