namespace domain
{
    public enum GuestRole
    {
        Host,
        Doorman,
        Guest
    }
}
