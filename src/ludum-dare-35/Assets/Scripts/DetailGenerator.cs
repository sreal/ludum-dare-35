using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using domain;

public static class DetailGenerator
{
    static int nameIndex = 0;
    static int imageIndex = 0;
    static IEnumerable<string> names = new string [] { "Garland Gleaves", "Javier Janzen", "Erwin Ethier", "Herb Hamburg", "Jerrold Jeanes", "Morton Mckim", "Jack Jeanlouis", "Terrence Toner", "Allen Agee", "Aaron Amezquita", "Norberto Nakashima", "Shon Stitt", "Tony Travis", "Reynaldo Rexroad", "Milton Musser", "Antony Armstead", "Abdul Avis", "Kermit Kwon", "Alonso Amante", "Kirk Kinchen", "Billye Barrington", "Sage Swigart", "Nella Nuttall", "Jackqueline Jefferys", "Charleen Carbaugh", "Lavonne Larocca", "Sherill Soden", "Gearldine Guido", "Shelli Scouten", "Hassie Hardrick", "Estela Edgell", "Suzette Schauer", "Yuko Yokley", "Margaretta Mckeel", "Sheryll Swindell", "Isa Inks", "Dominga Deherrera", "Kaitlin Knox", "Marilyn Mahony", "Bethel Bingaman" };
    static IEnumerable<string> images = new string [] { "guest_0", "guest_1", "guest_2", "guest_3", "guest_4", "guest_5" };
    static string[] currentNames = new string[0];
    static string[] currentImages = new string[0];



    public static string GetLoss(Person person)
    {
        return GetTextResource("loss", person.Role);
    }

    public static string GetWin(Person person)
    {
        var message = GetTextResource("win", person.Role);
        if (person.Role == GuestRole.Guest)
            message = String.Format(message, person.Name);
        return message;
    }

    public static string GetSummary(Person person)
    {
        var message = GetTextResource("summary", person.Role);
        if (person.Role == GuestRole.Guest)
            message = String.Format(message, person.Partner.Name);
        // if (person.IsShifter)
        //     message += "\n\nShhh. I'm the Shifter.";
        return message;
    }

    public static string GetImage(GuestRole role)
    {

        switch (role)
        {
        case GuestRole.Host:
            return "host";
        case GuestRole.Doorman:
            return "doorman";
        default:
            return MakeImage();
        }
    }
    static string MakeImage()
    {
        if (imageIndex == 0)
            currentImages = images.OrderBy(_ => Guid.NewGuid()).ToArray();

        var image = currentImages[imageIndex];
        imageIndex++;
        imageIndex = imageIndex % currentImages.Length;
        return image;
    }

    public static string GetName(GuestRole role)
    {
        switch (role)
        {
            case GuestRole.Host:
                return "The Host";
            case GuestRole.Doorman:
                return "The Doorman";
            default:
                return MakeName();
        }
    }
    static string MakeName()
    {
        if (nameIndex == 0)
            currentNames = names.OrderBy(_ => Guid.NewGuid()).ToArray();

        var name = currentNames[nameIndex];
        nameIndex++;
        nameIndex = nameIndex % currentNames.Length;
        return name;
    }

    static string GetTextResource(string type, GuestRole role)
    {
        return Resources.Load<TextAsset>(String.Format("Texts/{0}_{1}", type, role.ToString().ToLowerInvariant())).text;
    }
}
