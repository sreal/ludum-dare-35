﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using domain;
using System;
using System.Linq;


public class GameManager : MonoBehaviour
{
    public GameObject Profile;
    GameObject[] GuestButtons;
    public GameObject HostButton;
    public GameObject DoormanButton;

    public Text DebugText;
    PersonFactory personFactory;
    PartyFactory partyFactory;
    Party party;

    Person currentProfilePerson;

    void Awake()
    {
        partyFactory = new PartyFactory(new PersonFactory());
        party = partyFactory.Create();
        party.Shifter.WrongPartner = true;
        party.InitalizeShifter();

        GuestButtons = FindObjectsOfType<GameObject>().Where(x => x.name == "GuestButton").ToArray();
    }

    void LoadGuestButtons()
    {
        HostButton.GetComponent<GuestButton>().SetUser(party.Host);
        DoormanButton.GetComponent<GuestButton>().SetUser(party.Doorman);

        for (var i = 0; i < party.Guests.Count; i++) {
            GuestButtons[i].GetComponent<GuestButton>().SetUser(party.Guests[i]);
        }
    }

    public void LoadProfile(Person person)
    {
        currentProfilePerson = person;
        Profile.GetComponent<Profile>().SetUser(person);
    }

    public void Accuse()
    {
        if (currentProfilePerson.Id == party.Shifter.Id)
            Profile.GetComponent<Profile>().SetUserAsWin(currentProfilePerson);
        else
            Profile.GetComponent<Profile>().SetUserAsLoss(currentProfilePerson);
    }

    void Start()
    {
        // if (party == null)
        //     throw new Exception("party is null");
        // if (DebugText == null)
        //     throw new Exception("DebugText is null");
        // DebugText.text = party.DebugParty();

        LoadGuestButtons();
        LoadProfile(party.Host);
    }

}
